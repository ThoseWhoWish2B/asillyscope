
import socket as sock      # Socks keep feet warm
import threading
import matplotlib.pyplot as plt
import numpy as np
import msvcrt as ms

# cHEADER = '\x00\xF0\x00\xF0'
# cSIZE_HEADER = len(cHEADER)

cSIZE_DATA = 4000

udp = sock.socket(sock.AF_INET, sock.SOCK_DGRAM)
udp.settimeout(3)
udp.bind(('',6666))
dest = ('192.168.4.1', 6666)

def sendData():
	while True:
		udp.sendto ((input()).encode(), dest)

def recvData():
	while True:
		msg, (cIp, cPort) = udp.recvfrom(cSIZE_DATA)
		print (str(msg))

fig = plt.figure()
screen = fig.add_subplot(111)
x = np.arange(0,int(cSIZE_DATA/2), dtype=np.int)
y = np.ones(int(cSIZE_DATA/2), dtype=np.int)
line, = screen.plot(x, y, '*-')

plt.ion()
fig.gca().set_ylim([-1000,5000])

print("Looping the loop\nPress a key to break freee...")

while not ms.kbhit():
		
	try:
		B = []
		B, (cIp, cPort) = udp.recvfrom(cSIZE_DATA)
	except:
		print("Communication break down, baby I'm gonna leave you...")
		udp.close()
		quit()
		
	else:
		#B_split = re.findall('..', B)
		#print(B)
		
		B_split = [ B[i:i+2] for i in range(0, len(B), 2) ]
		B_int = [ int(B_split[i][1])<<8+int(B_split[i][0]) for i in range(0, len(B_split)-1) ]
		
		x = np.linspace(0, len(B_int)-1, len(B_int))
		#x = np.arange(0,len(B_int), dtype=np.int)	# packet may be smaller than expected
		
		#int(B_split[0][1].encode('hex'),16)*256+int(B_split[0][0].encode('hex'),16)
		#print(B_split)
		#print(B_int)
		
		#line.set_data(x,B_int)
		plt.clf()
		plt.plot(x, B_int)
		
		plt.draw()
		plt.show()
		
		plt.pause(.0001)
		
		#plt.clf()
		#plt.plot(B_int, '-*')
		#plt.gca().set_ylim([-5,5000])
		#plt.draw()