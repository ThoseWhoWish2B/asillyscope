/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f3xx_hal.h"

/* USER CODE BEGIN Includes */

// Canal 1 wont trigger by hysteresis

// TIM8 -> trigger ADCs
// TIM7 -> counter for time between Trigger event and send
// TIM2 (PA0) -> PWM for debug
// ADC2_IN7 -> PC1
// ADC3_IN5 -> PB13
// GREEN LED -> PA5

#include <string.h>

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc2;
ADC_HandleTypeDef hadc3;
DMA_HandleTypeDef hdma_adc2;
DMA_HandleTypeDef hdma_adc3;

COMP_HandleTypeDef hcomp5;
COMP_HandleTypeDef hcomp7;

DAC_HandleTypeDef hdac1;

TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim7;
TIM_HandleTypeDef htim8;

UART_HandleTypeDef huart2;
UART_HandleTypeDef huart3;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

//#define __DEBUG__

const uint32_t cMAGIC = 0xABBA;  // gimme, gimme, gimme a man after midnight...
const uint16_t cSIZE_GENERAL_HEADER = 8;

const int32_t cFREQ_CLK = 72000000; // clock freq in Hz

const int16_t cSIZE_ADC_BUFFER = 8000;	// must be even
const int16_t cPTS_PER_SCRN    = 2500;

const int32_t cFREQ_SAMPLE = 4000000; // sampling frequency in Hz
const int32_t cADC_TRGR_TIM_PRESCALER = 3-1;

const uint32_t cADC_TRGR_TIM_PERIOD = cFREQ_CLK/((cADC_TRGR_TIM_PRESCALER+1)*(cFREQ_SAMPLE)) - 1;  // make sure it is an integer

const uint32_t cSHADOW_GRANULARITY = 3;
//const uint32_t cSHADOW_GRANULARITY = 2; //da ruim
const uint32_t cADC_SHADOW_TIM_PRESCALER = ((cADC_TRGR_TIM_PERIOD+1)*(cADC_TRGR_TIM_PRESCALER+1))/cSHADOW_GRANULARITY-1;   // make sure it is an integer

//tGeneralHeader
typedef struct{
	uint32_t magic;	// erweitern mit verstärkung, pps, auslöserstelle aufm bildschirm, und sonstiges
	uint16_t state; // each bit: (..., ch1 on, ch2 on)
	int16_t  pos_trigger_screen;
}tGeneralHeader;

//tChannel
typedef struct{
	uint8_t  gain;  // enumeration, from smallest to biggest
	uint8_t  COMP_IRQn;
	COMP_HandleTypeDef *hcomp;
	ADC_HandleTypeDef  *hadc;
	uint16_t  buffer_adc[cSIZE_ADC_BUFFER];
	uint16_t  buffer_screen[cPTS_PER_SCRN];
}tChannel;

//tTrigger
typedef struct{
	tChannel *channel;
	TIM_HandleTypeDef  *hshadow_timer;
	uint8_t STIM_IRQn;
}tTrigger;

tGeneralHeader oszi_header;
tGeneralHeader GUI_header;

tChannel channel_1;
tChannel channel_2;

tTrigger trigger;

uint32_t go_bytes = 0x00000001;

uint16_t level_trigger = 2048;

int16_t pos_trigger    = 0;
int16_t pos_pivot 		 = 0;
int16_t pos_checkpoint = 0;

//uint16_t pos_trigger_screen = cPTS_PER_SCRN/2;
//uint16_t pos_trigger_screen_temp = cPTS_PER_SCRN/2;
uint16_t pos_trigger_screen = cPTS_PER_SCRN-1;
uint16_t pos_trigger_screen_temp = cPTS_PER_SCRN-1;

uint32_t tickstart = 0;
uint8_t is_usb_buffer_ok = 0;

char message[] = "i am yr father\n";

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_ADC2_Init(void);
static void MX_ADC3_Init(void);
static void MX_DAC1_Init(void);
static void MX_COMP7_Init(void);
static void MX_TIM8_Init(void);
static void MX_COMP5_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM7_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_NVIC_Init(void);

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);
                                

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

uint16_t WrapIndex(int16_t pos_pivot, uint16_t buffer_size)
{
	int16_t remainder = pos_pivot%buffer_size;
	
	if(remainder<0)
	{
		return (uint16_t)(remainder+buffer_size);
	}
	else
	{
		return (uint16_t)remainder;
	}
}

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */
	
		// Initialize variables, channels, etc...
		oszi_header.magic = cMAGIC;
		
		channel_1.gain = 0;
		channel_1.hcomp = &hcomp7;
		channel_1.COMP_IRQn = COMP7_IRQn;
		channel_1.hadc = &hadc2;
		
		channel_2.gain = 0;
		channel_2.hcomp = &hcomp5;
		channel_2.COMP_IRQn = COMP4_5_6_IRQn;
		channel_2.hadc = &hadc3;
		
		trigger.channel = &channel_1;
		trigger.hshadow_timer = &htim7;
		trigger.STIM_IRQn = TIM7_IRQn;
  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  MX_ADC2_Init();
  MX_ADC3_Init();
  MX_DAC1_Init();
  MX_COMP7_Init();
  MX_TIM8_Init();
  MX_COMP5_Init();
  MX_TIM2_Init();
  MX_TIM7_Init();
  MX_USART3_UART_Init();

  /* Initialize interrupts */
  MX_NVIC_Init();

  /* USER CODE BEGIN 2 */
	
	// set up some cofigurations:
	__HAL_TIM_SET_PRESCALER(&htim7,cADC_SHADOW_TIM_PRESCALER);
	
	__HAL_TIM_SET_PRESCALER(&htim8,cADC_TRGR_TIM_PRESCALER);
	__HAL_TIM_SET_AUTORELOAD(&htim8, cADC_TRGR_TIM_PERIOD);
	
	// start up some peripherals
	HAL_ADC_Start_DMA(&hadc2, (uint32_t*)channel_1.buffer_adc, cSIZE_ADC_BUFFER);
	HAL_ADC_Start_DMA(&hadc3, (uint32_t*)channel_2.buffer_adc, cSIZE_ADC_BUFFER);
	
	HAL_TIM_Base_Start(&htim8);
	HAL_TIM_Base_Start(&htim2);
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
	
	HAL_DAC_SetValue(&hdac1, DAC_CHANNEL_1, DAC_ALIGN_12B_R, level_trigger);
	HAL_DAC_Start(&hdac1, DAC_CHANNEL_1);
	HAL_COMP_Start_IT(&hcomp5);
	HAL_COMP_Start_IT(&hcomp7);
	
	//HAL_UART_Receive_IT(&huart2, (uint8_t*)&GUI_header, cSIZE_GENERAL_HEADER);
	//HAL_UART_Receive_IT(&huart2, (uint8_t*)&go_bytes, 4);
			
	HAL_UART_Receive_IT(&huart2, (uint8_t*)&go_bytes, 4);
	
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		if(HAL_GetTick() - tickstart > 20)
		//if(HAL_GetTick() - tickstart > 50)
		{
			if(is_usb_buffer_ok)
			{		
				if(go_bytes == cMAGIC)
				{
					go_bytes = 0;   // receive permission every time
					GPIOA->ODR |= GPIO_PIN_5;
					
					#ifndef __DEBUG__
						// Send oszi_header for the transmission:
						HAL_UART_Transmit(&huart2, (uint8_t*)&oszi_header.magic, cSIZE_GENERAL_HEADER, 100);
						HAL_Delay(1);
						//HAL_UART_Transmit(&huart2, (uint8_t*)trigger.channel->buffer_screen, 2*cPTS_PER_SCRN, 100);
						HAL_UART_Transmit(&huart2, (uint8_t*)channel_1.buffer_screen, 2*cPTS_PER_SCRN, 100);
						HAL_Delay(1);
						HAL_UART_Transmit(&huart2, (uint8_t*)channel_2.buffer_screen, 2*cPTS_PER_SCRN, 100);
					#else
						HAL_UART_Transmit(&huart2, (uint8_t*)&go_bytes, sizeof(go_bytes), 100);
					#endif //not __DEBUG__
					
					GPIOA->ODR &= ~GPIO_PIN_5;
				}
				
				is_usb_buffer_ok = 0;
				
				COMP_EXTI_CLEAR_FLAG(COMP_GET_EXTI_LINE(trigger.channel->hcomp->Instance));
				HAL_NVIC_EnableIRQ(trigger.channel->COMP_IRQn);
			}
			
			// dummy trigger position decrement...
			if(!HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_13))
			{
				int16_t dummy = pos_trigger_screen_temp - 100;
				
				// test underflow
				if( dummy < (cPTS_PER_SCRN-cSIZE_ADC_BUFFER+1) )
				{
					dummy = cPTS_PER_SCRN-1;
				}
				
				pos_trigger_screen_temp = dummy;
			}
			
			// update hardware configurations if any received:
			
			tickstart = HAL_GetTick();
		}
		
		HAL_Delay(1);
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_USART3
                              |RCC_PERIPHCLK_TIM8|RCC_PERIPHCLK_ADC12
                              |RCC_PERIPHCLK_ADC34|RCC_PERIPHCLK_TIM2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
  PeriphClkInit.Adc12ClockSelection = RCC_ADC12PLLCLK_DIV1;
  PeriphClkInit.Adc34ClockSelection = RCC_ADC34PLLCLK_DIV1;
  PeriphClkInit.Tim8ClockSelection = RCC_TIM8CLK_HCLK;
  PeriphClkInit.Tim2ClockSelection = RCC_TIM2CLK_HCLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/** NVIC Configuration
*/
static void MX_NVIC_Init(void)
{
  /* TIM7_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM7_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(TIM7_IRQn);
  /* DMA2_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Channel1_IRQn);
  /* DMA2_Channel5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Channel5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Channel5_IRQn);
  /* COMP4_5_6_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(COMP4_5_6_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(COMP4_5_6_IRQn);
  /* COMP7_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(COMP7_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(COMP7_IRQn);
  /* USART2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USART2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(USART2_IRQn);
  /* USART3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(USART3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(USART3_IRQn);
}

/* ADC2 init function */
static void MX_ADC2_Init(void)
{

  ADC_ChannelConfTypeDef sConfig;

    /**Common config 
    */
  hadc2.Instance = ADC2;
  hadc2.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc2.Init.Resolution = ADC_RESOLUTION_12B;
  hadc2.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc2.Init.ContinuousConvMode = DISABLE;
  hadc2.Init.DiscontinuousConvMode = ENABLE;
  hadc2.Init.NbrOfDiscConversion = 1;
  hadc2.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_RISING;
  hadc2.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T8_TRGO;
  hadc2.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc2.Init.NbrOfConversion = 1;
  hadc2.Init.DMAContinuousRequests = ENABLE;
  hadc2.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc2.Init.LowPowerAutoWait = DISABLE;
  hadc2.Init.Overrun = ADC_OVR_DATA_OVERWRITTEN;
  if (HAL_ADC_Init(&hadc2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_7;
  sConfig.Rank = 1;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc2, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* ADC3 init function */
static void MX_ADC3_Init(void)
{

  ADC_MultiModeTypeDef multimode;
  ADC_ChannelConfTypeDef sConfig;

    /**Common config 
    */
  hadc3.Instance = ADC3;
  hadc3.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc3.Init.Resolution = ADC_RESOLUTION_12B;
  hadc3.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc3.Init.ContinuousConvMode = DISABLE;
  hadc3.Init.DiscontinuousConvMode = ENABLE;
  hadc3.Init.NbrOfDiscConversion = 1;
  hadc3.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_RISING;
  hadc3.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T8_TRGO;
  hadc3.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc3.Init.NbrOfConversion = 1;
  hadc3.Init.DMAContinuousRequests = ENABLE;
  hadc3.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc3.Init.LowPowerAutoWait = DISABLE;
  hadc3.Init.Overrun = ADC_OVR_DATA_OVERWRITTEN;
  if (HAL_ADC_Init(&hadc3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the ADC multi-mode 
    */
  multimode.Mode = ADC_MODE_INDEPENDENT;
  if (HAL_ADCEx_MultiModeConfigChannel(&hadc3, &multimode) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_5;
  sConfig.Rank = 1;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc3, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* COMP5 init function */
static void MX_COMP5_Init(void)
{

  hcomp5.Instance = COMP5;
  hcomp5.Init.InvertingInput = COMP_INVERTINGINPUT_DAC1_CH1;
  hcomp5.Init.NonInvertingInput = COMP_NONINVERTINGINPUT_IO1;
  hcomp5.Init.Output = COMP_OUTPUT_NONE;
  hcomp5.Init.OutputPol = COMP_OUTPUTPOL_NONINVERTED;
  hcomp5.Init.BlankingSrce = COMP_BLANKINGSRCE_NONE;
  hcomp5.Init.TriggerMode = COMP_TRIGGERMODE_IT_RISING;
  if (HAL_COMP_Init(&hcomp5) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* COMP7 init function */
static void MX_COMP7_Init(void)
{

  hcomp7.Instance = COMP7;
  hcomp7.Init.InvertingInput = COMP_INVERTINGINPUT_DAC1_CH1;
  hcomp7.Init.NonInvertingInput = COMP_NONINVERTINGINPUT_IO1;
  hcomp7.Init.Output = COMP_OUTPUT_NONE;
  hcomp7.Init.OutputPol = COMP_OUTPUTPOL_NONINVERTED;
  hcomp7.Init.BlankingSrce = COMP_BLANKINGSRCE_NONE;
  hcomp7.Init.TriggerMode = COMP_TRIGGERMODE_IT_RISING;
  if (HAL_COMP_Init(&hcomp7) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* DAC1 init function */
static void MX_DAC1_Init(void)
{

  DAC_ChannelConfTypeDef sConfig;

    /**DAC Initialization 
    */
  hdac1.Instance = DAC1;
  if (HAL_DAC_Init(&hdac1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**DAC channel OUT1 config 
    */
  sConfig.DAC_Trigger = DAC_TRIGGER_SOFTWARE;
  sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
  if (HAL_DAC_ConfigChannel(&hdac1, &sConfig, DAC_CHANNEL_1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**DAC channel OUT2 config 
    */
  sConfig.DAC_Trigger = DAC_TRIGGER_NONE;
  if (HAL_DAC_ConfigChannel(&hdac1, &sConfig, DAC_CHANNEL_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM2 init function */
static void MX_TIM2_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;

  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 199;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 99;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 50;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  HAL_TIM_MspPostInit(&htim2);

}

/* TIM7 init function */
static void MX_TIM7_Init(void)
{

  TIM_MasterConfigTypeDef sMasterConfig;

  htim7.Instance = TIM7;
  htim7.Init.Prescaler = 143;
  htim7.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim7.Init.Period = 65535;
  htim7.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim7) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim7, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM8 init function */
static void MX_TIM8_Init(void)
{

  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim8.Instance = TIM8;
  htim8.Init.Prescaler = 0;
  htim8.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim8.Init.Period = 719;
  htim8.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim8.Init.RepetitionCounter = 0;
  htim8.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_Base_Init(&htim8) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim8, &sClockSourceConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim8, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 2000000;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART3 init function */
static void MX_USART3_UART_Init(void)
{

  huart3.Instance = USART3;
  huart3.Init.BaudRate = 2000000;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, MUX1_bit0_Pin|MUX_bit1_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, MUX2_bit0_Pin|MUX2_bit1_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : AC_DC_CH1_Pin */
  GPIO_InitStruct.Pin = AC_DC_CH1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(AC_DC_CH1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : AC_DC_CH1A1_Pin */
  GPIO_InitStruct.Pin = AC_DC_CH1A1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(AC_DC_CH1A1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PB15 */
  GPIO_InitStruct.Pin = GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : MUX1_bit0_Pin MUX_bit1_Pin */
  GPIO_InitStruct.Pin = MUX1_bit0_Pin|MUX_bit1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : MUX2_bit0_Pin MUX2_bit1_Pin */
  GPIO_InitStruct.Pin = MUX2_bit0_Pin|MUX2_bit1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

void HAL_COMP_TriggerCallback(COMP_HandleTypeDef *hcomp)
{
	//if(hcomp==&hcomp5)	
	//if((hcomp==trigger.channel->hcomp)&&(buffer_adc[hadc2.DMA_Handle->Instance->CNDTR-1]-buffer_adc2[hadc2.DMA_Handle->Instance->CNDTR-4]>40))
	if(hcomp==trigger.channel->hcomp)
	{	
		uint16_t pos_trigger_temp = cSIZE_ADC_BUFFER - trigger.channel->hadc->DMA_Handle->Instance->CNDTR - 1 - 4; // -4 to compensate for IRQ latency...
		
		//if( (trigger.channel->buffer_adc[pos_trigger_temp] - trigger.channel->buffer_adc[pos_trigger_temp-10])>40 )
		//if( (trigger.channel->buffer_adc[pos_trigger_temp]) > (level_trigger+100) )
		{			
			pos_trigger = pos_trigger_temp;
			
			pos_pivot = pos_trigger - pos_trigger_screen;
			pos_pivot = WrapIndex(pos_pivot, cSIZE_ADC_BUFFER);
			
			pos_checkpoint = pos_pivot + cPTS_PER_SCRN;
			pos_checkpoint = WrapIndex(pos_checkpoint, cSIZE_ADC_BUFFER);
			
			int32_t counts_to_send_temp = cSHADOW_GRANULARITY*(cPTS_PER_SCRN - pos_trigger_screen);
			//if(counts_to_send_temp > 0)
			{
				trigger.hshadow_timer->Instance->ARR = counts_to_send_temp;
				__HAL_TIM_CLEAR_IT(trigger.hshadow_timer, TIM_FLAG_UPDATE);
				
				//GPIOA->ODR &= ~GPIO_PIN_5;
			}
			//else
			//{
			//	trigger.hshadow_timer->Instance->SR |= TIM_FLAG_UPDATE;
			//	
			//	//GPIOA->ODR |= GPIO_PIN_5;
			//}
			
			HAL_NVIC_DisableIRQ(trigger.channel->COMP_IRQn);
			
			// turn on interrupts
			HAL_NVIC_EnableIRQ(trigger.STIM_IRQn);
			HAL_TIM_Base_Start_IT(trigger.hshadow_timer);
			
			//char message2[10] = "comp!";
			//HAL_UART_Transmit(&huart2, message2, strlen(message2), 100);
		}
	}
}

//void HAL_TIM_OC_DelayElapsedCallback(TIM_HandleTypeDef *htim)
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim == trigger.hshadow_timer)
	{
		HAL_NVIC_DisableIRQ(trigger.STIM_IRQn);
		HAL_TIM_Base_Stop_IT(trigger.hshadow_timer);
		trigger.hshadow_timer->Instance->CNT = 0;
		
		uint16_t samples_sent = 0;
		
		if((pos_pivot+cPTS_PER_SCRN) >= cSIZE_ADC_BUFFER)
		{
			samples_sent = cSIZE_ADC_BUFFER-pos_pivot;
		}
		else
		{
			samples_sent = cPTS_PER_SCRN;
		}
		
		memcpy(channel_1.buffer_screen, &channel_1.buffer_adc[pos_pivot], 2*samples_sent);
		memcpy(channel_2.buffer_screen, &channel_2.buffer_adc[pos_pivot], 2*samples_sent);
		
		if(samples_sent < cPTS_PER_SCRN)
		{
			memcpy(&channel_1.buffer_screen[samples_sent], channel_1.buffer_adc, 2*pos_checkpoint);
			memcpy(&channel_2.buffer_screen[samples_sent], channel_2.buffer_adc, 2*pos_checkpoint);
		}
		
		is_usb_buffer_ok = 1;
		
		pos_trigger_screen = pos_trigger_screen_temp;
		oszi_header.pos_trigger_screen = pos_trigger_screen;
		
		//char message3[10] = "timer!";
		//HAL_UART_Transmit(&huart2, message3, strlen(message3), 100);
	}
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void _Error_Handler(char * file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
