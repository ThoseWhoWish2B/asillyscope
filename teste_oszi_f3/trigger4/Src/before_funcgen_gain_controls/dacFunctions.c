/*
 * dacFunctions.c
 *
 *  Created on: 18 de out de 2018
 *      Author: jvpre
 */

#include "dacFunctions.h"

uint16_t dacBuffer[DAC_BUFFER_SIZE];

void generator_init(TIM_HandleTypeDef *htim){
  HAL_DAC_Start_DMA(&hdac1, DAC_CHANNEL_2, (uint32_t*)dacBuffer, DAC_BUFFER_SIZE, DAC_ALIGN_12B_R);
  HAL_TIM_Base_Start(htim);
}
void generator_generate(uint16_t div, uint16_t n, uint16_t vp, uint16_t of, TIM_HandleTypeDef htim){
	for (int i=0; i!=DAC_BUFFER_SIZE; i++)
		dacBuffer[i]=(uint16_t)((double)of+((double)vp)*(sin(((double)n)*2.0*3.14159265359*((double)i)/DAC_BUFFER_SIZE)));
	htim.Instance->ARR=div;
}
