/*
 * scopeCore.h
 *
 *  Created on: 19 de out de 2018
 *      Author: w_becker
 */

#ifndef SCOPECORE_H_
#define SCOPECORE_H_

#include "stm32f3xx_hal.h"
#include <string.h>

#define cMAGIC 0xABBA              // gimme, gimme, gimme a man after midnight...
#define cSIZE_GENERAL_HEADER 8
#define cFREQ_CLK 72000000         // clock freq in Hz
#define cSIZE_ADC_BUFFER 8000	   // must be even
#define cPTS_PER_SCRN    2500
#define cDEFAULT_SAMPLING_FREQ 4000000
#define cADC_TRGR_TIM_PRESCALER 3-1
#define cSHADOW_GRANULARITY 3

// Masks for state byte
#define cMSK_CH1_ON  0x01
#define cMSK_CH2_ON  0x02
#define cMSK_CH1_AC  0x04
#define cMSK_CH2_AC  0x08
#define cMSK_TGR_SRC 0x10
#define cMSK_OPER    0x60
#define cVAL_STOP    0x00
#define cVAL_RUN     0x20
#define cVAL_SINGLE  0x40
#define cVAL_ROLL    0x60

//tGeneralHeader
typedef struct{
	uint16_t magic;	// erweitern mit verstaerkung, pps, ausloeserstelle aufm bildschirm, und sonstiges
	uint16_t padding1;
	int16_t  pos_trigger_screen;
	uint8_t  padding2;
	uint8_t  state; // each bit: (..., ch1 on, ch2 on)
}tGeneralHeader;

//tChannel
typedef struct{
	uint8_t  gain;  // enumeration, from smallest to biggest
	uint8_t  COMP_IRQn;
	COMP_HandleTypeDef *hcomp;
	ADC_HandleTypeDef  *hadc;
	uint16_t*  buffer_adc;
	uint16_t*  buffer_screen;
}tChannel;

//tTrigger
typedef struct{
	tChannel *channel;
	TIM_HandleTypeDef  *hshadow_timer;
	TIM_HandleTypeDef  *hdebounce_timer;
	uint8_t  STIM_IRQn;
	uint8_t  DTIM_IRQn;
	uint16_t pos_buffer;
	uint16_t pos_screen;
}tTrigger;

// Functions
uint16_t WrapIndex(int16_t pos_pivot, uint16_t buffer_size);
//extern void HAL_COMP_TriggerCallback(COMP_HandleTypeDef *hcomp);
//extern void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);

// Variables
extern tGeneralHeader header_oszi;
extern tGeneralHeader header_GUI;
extern tChannel channel_1;
extern tChannel channel_2;
extern tTrigger trigger;
extern uint32_t go_bytes;
extern uint16_t level_trigger;
extern int16_t pos_trigger;
extern int16_t pos_pivot;
extern int16_t pos_checkpoint;

extern int32_t freq_sample;

extern uint16_t pos_trigger_screen;
extern uint16_t pos_trigger_screen_temp;

extern uint32_t tickstart;
extern uint8_t is_usb_buffer_ok;

#endif  //SCOPECORE_H_
