/*
 * scopeCore.h
 *
 *  Created on: 19 de out de 2018
 *      Author: w_becker
 */

 #include "scopeCore.h"

// Variables:
tGeneralHeader header_oszi;
tGeneralHeader header_GUI;

tChannel channel_1;
tChannel channel_2;

tTrigger trigger;

uint32_t go_bytes = 0x00000001;

uint16_t level_trigger = 2048;

int16_t pos_pivot      = 0;
int16_t pos_checkpoint = 0;

int32_t freq_sample = cDEFAULT_SAMPLING_FREQ; // sampling frequency in Hz

uint16_t pos_trigger_screen_temp = cPTS_PER_SCRN/2;

uint32_t tickstart = 0;
uint8_t is_usb_buffer_ok = 0;

// Function implementations:
uint16_t WrapIndex(int16_t pos_pivot, uint16_t buffer_size)
{
	int16_t remainder = pos_pivot%buffer_size;

	if(remainder<0)
	{
		return (uint16_t)(remainder+buffer_size);
	}
	else
	{
		return (uint16_t)remainder;
	}
}

void HAL_COMP_TriggerCallback(COMP_HandleTypeDef *hcomp)   // COMP_HandleTypeDef *hcomp
{
	if(hcomp==trigger.channel->hcomp)
	{
		GPIOB->ODR ^= GPIO_PIN_4;

		//if((  trigger.channel->hcomp->Instance->CSR & 0x40000000) && 1) // substitute 1 by flank polarity
		uint16_t pos_trigger_temp = cSIZE_ADC_BUFFER - trigger.channel->hadc->DMA_Handle->Instance->CNDTR - 1;
		trigger.pos_buffer = pos_trigger_temp - 4;   // -4 to compensate for IRQ latency

	    if(trigger.channel->buffer_adc[pos_trigger_temp] > (level_trigger + 80))
		{
			pos_pivot = trigger.pos_buffer - trigger.pos_screen;
			pos_pivot = WrapIndex(pos_pivot, cSIZE_ADC_BUFFER);

			pos_checkpoint = pos_pivot + cPTS_PER_SCRN;
			pos_checkpoint = WrapIndex(pos_checkpoint, cSIZE_ADC_BUFFER);

			int32_t counts_to_send_temp = cSHADOW_GRANULARITY*(cPTS_PER_SCRN - trigger.pos_screen);
			{
				trigger.hshadow_timer->Instance->ARR = counts_to_send_temp;
			}
			HAL_NVIC_DisableIRQ(trigger.channel->COMP_IRQn);

			// turn on interrupts
			HAL_NVIC_EnableIRQ(trigger.STIM_IRQn);
			HAL_TIM_Base_Start_IT(trigger.hshadow_timer);
		}

		// turn on interrupts
		//HAL_NVIC_EnableIRQ(trigger.DTIM_IRQn);
		//HAL_TIM_Base_Start_IT(trigger.hdebounce_timer);
	}
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim == trigger.hdebounce_timer)
	{
		uint16_t pos_trigger_temp = cSIZE_ADC_BUFFER - trigger.channel->hadc->DMA_Handle->Instance->CNDTR - 1;

		if(trigger.channel->buffer_adc[pos_trigger_temp] > (level_trigger + 40))
		{
			pos_pivot = trigger.pos_buffer - trigger.pos_screen;
			pos_pivot = WrapIndex(pos_pivot, cSIZE_ADC_BUFFER);

			pos_checkpoint = pos_pivot + cPTS_PER_SCRN;
			pos_checkpoint = WrapIndex(pos_checkpoint, cSIZE_ADC_BUFFER);

			int32_t counts_to_send_temp = cSHADOW_GRANULARITY*(cPTS_PER_SCRN - trigger.pos_screen);
			{
				trigger.hshadow_timer->Instance->ARR = counts_to_send_temp;
			}
			HAL_NVIC_DisableIRQ(trigger.channel->COMP_IRQn);

			// turn on interrupts
			HAL_NVIC_EnableIRQ(trigger.STIM_IRQn);
			HAL_TIM_Base_Start_IT(trigger.hshadow_timer);
		}
	}
	else if(htim == trigger.hshadow_timer)
	{
		HAL_NVIC_DisableIRQ(trigger.STIM_IRQn);
		HAL_TIM_Base_Stop_IT(trigger.hshadow_timer);
		trigger.hshadow_timer->Instance->CNT = 0;

		uint16_t samples_sent = 0;

		if((pos_pivot+cPTS_PER_SCRN) >= cSIZE_ADC_BUFFER)
		{
			samples_sent = cSIZE_ADC_BUFFER-pos_pivot;
		}
		else
		{
			samples_sent = cPTS_PER_SCRN;
		}

		memcpy(channel_1.buffer_screen, &channel_1.buffer_adc[pos_pivot], 2*samples_sent);
		memcpy(channel_2.buffer_screen, &channel_2.buffer_adc[pos_pivot], 2*samples_sent);

		if(samples_sent < cPTS_PER_SCRN)
		{
			memcpy(&channel_1.buffer_screen[samples_sent], channel_1.buffer_adc, 2*pos_checkpoint);
			memcpy(&channel_2.buffer_screen[samples_sent], channel_2.buffer_adc, 2*pos_checkpoint);
		}

		is_usb_buffer_ok = 1;

		trigger.pos_screen = header_oszi.pos_trigger_screen;
		//trigger.pos_screen = pos_trigger_screen_temp;
		//oszi_header.pos_trigger_screen = trigger.pos_screen;
	}
}
