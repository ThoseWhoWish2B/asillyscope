/*
 * dacFunctions.c
 *
 *  Created on: 18 de out de 2018
 *      Author: jvpre
 */

#include "dacFunctions.h"

uint16_t dacBuffer[DAC_BUFFER_SIZE];

void generator_init(TIM_HandleTypeDef *htim){
  HAL_DAC_Start_DMA(&hdac1, DAC_CHANNEL_2, (uint32_t*)dacBuffer, DAC_BUFFER_SIZE, DAC_ALIGN_12B_R);
  HAL_TIM_Base_Start(htim);
}
void generator_deinit(){
  HAL_DAC_Stop_DMA(&hdac1, DAC_CHANNEL_2);
}
void generator_generate(uint16_t div, uint16_t n, uint16_t vp, uint16_t of, TIM_HandleTypeDef htim){
	static int16_t temp = 0;
	for (int i=0; i!=DAC_BUFFER_SIZE; i++)
	{
		temp = (int16_t)((double)of+((double)vp)*(sin(((double)n)*2.0*3.14159265359*((double)i)/DAC_BUFFER_SIZE)));

		if(temp>4000)
			temp = 4000;
		else if(temp<100)
			temp = 100;

		dacBuffer[i] = (uint16_t)temp;
	}
	htim.Instance->ARR=div;
}
