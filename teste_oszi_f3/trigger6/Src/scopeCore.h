/*
 * scopeCore.h
 *
 *  Created on: 19 de out de 2018
 *      Author: w_becker
 */

#ifndef SCOPECORE_H_
#define SCOPECORE_H_

#include "stm32f3xx_hal.h"
#include <string.h>

#define cMAGIC 0xABBA              // gimme, gimme, gimme a man after midnight...
#define cSIZE_GENERAL_HEADER 16
#define cFREQ_CLK 72000000         // clock freq in Hz
#define cSIZE_ADC_BUFFER 8000	   // must be even
#define cPTS_PER_SCRN    2500
#define cDEFAULT_SAMPLING_FREQ 4000000
#define cADC_TRGR_TIM_PRESCALER 3-1
#define cSHADOW_GRANULARITY 3

// Masks for state byte
#define cMSK_CH1_ON  0x0001
#define cMSK_CH2_ON  0x0002
#define cMSK_FG_ON   0x0800
#define cMSK_CH1_AC  0x0004
#define cMSK_CH2_AC  0x0008
#define cMSK_CH1_GAIN  0x0030
#define cMSK_CH2_GAIN  0x00C0

#define cMSK_FG_AC   0x1000
#define cMSK_TGR_SRC 0x0400
#define cMSK_OPER    0x0300

#define cVAL_STOP    0x0000
#define cVAL_RUN     0x0100
#define cVAL_SINGLE  0x0200
#define cVAL_ROLL    0x0300

#define cVAL_CH1_X0_1 0x0000
#define cVAL_CH1_X01  0x0010
#define cVAL_CH1_X10  0x0020
#define cVAL_CH2_X0_1 0x0000
#define cVAL_CH2_X01  0x0040
#define cVAL_CH2_X10  0x0080

//tGeneralHeader
typedef struct{
	uint16_t magic;
	uint16_t fg_div;
	uint16_t fg_n;
	uint16_t fg_vp;
	uint16_t fg_of;
	int16_t  pos_trigger_screen;
	int16_t  level_trigger;
	uint16_t  state;
}tGeneralHeader;

//tChannel
typedef struct{
	uint8_t  gain;  // enumeration, from smallest to biggest
	uint8_t  COMP_IRQn;
	COMP_HandleTypeDef *hcomp;
	ADC_HandleTypeDef  *hadc;
	uint16_t*  buffer_adc;
	uint16_t*  buffer_screen;
}tChannel;

//tTrigger
typedef struct{
	tChannel *channel;
	TIM_HandleTypeDef  *hshadow_timer;
	TIM_HandleTypeDef  *hdebounce_timer;
	uint8_t  STIM_IRQn;
	uint8_t  DTIM_IRQn;
	uint16_t pos_buffer;
	uint16_t pos_screen;
	uint16_t level;
}tTrigger;

// Functions
uint16_t WrapIndex(int16_t pos_pivot, uint16_t buffer_size);
//extern void HAL_COMP_TriggerCallback(COMP_HandleTypeDef *hcomp);
//extern void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);

// Variables
extern tGeneralHeader header_oszi;
extern tGeneralHeader header_GUI;
extern tChannel channel_1;
extern tChannel channel_2;
extern tTrigger trigger;
extern uint32_t go_bytes;
extern uint16_t level_trigger;
extern int16_t pos_trigger;
extern int16_t pos_pivot;
extern int16_t pos_checkpoint;

extern int32_t freq_sample;

extern uint16_t pos_trigger_screen;
extern uint16_t pos_trigger_screen_temp;

extern uint32_t tickstart;
extern uint8_t is_usb_buffer_ok;

#endif  //SCOPECORE_H_
