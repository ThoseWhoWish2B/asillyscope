/*
 * dacFunctions.h
 *
 *  Created on: 18 de out de 2018
 *      Author: jvpre
 */

#ifndef DACFUNCTIONS_H_
#define DACFUNCTIONS_H_

#include "stm32f3xx_hal.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

#define DAC_BUFFER_SIZE 8192
extern uint16_t dacBuffer[DAC_BUFFER_SIZE];

extern DAC_HandleTypeDef hdac1;
//extern DMA_HandleTypeDef hdma_dac1_ch2;

void generator_init(TIM_HandleTypeDef *htim);
void generator_deinit(void);
void generator_generate(uint16_t div, uint16_t n, uint16_t vp, uint16_t of, TIM_HandleTypeDef htim);
#endif

/* DACFUNCTIONS_H_ */
