import matplotlib.pyplot as plt
import numpy as np
import time as tm
import serial as ps
import msvcrt as ms

cSIZE_DATA = 4000 # number of received bytes (2x points per screen)

#oszi = ps.Serial('COM19', 115200, timeout=1)
oszi = ps.Serial('COM19', 1000000, timeout=1)
#oszi = ps.Serial('COM19', 2000000)

# debug:
# counter = 0
# A = np.linspace(0,np.pi,50)
# B = np.sin(A+counter)
# /debug

fig = plt.figure()
screen = fig.add_subplot(111)
x = np.arange(0,int(cSIZE_DATA/2), dtype=np.int)
y = np.ones(int(cSIZE_DATA/2), dtype=np.int)
line, = screen.plot(x, y, '*-')

plt.ion()
fig.gca().set_ylim([-1000,5000])

print("Looping the loop\nPress a key to break freee...")

while not ms.kbhit():
	# debug
	#counter += np.pi/6
	#print counter
	#B = np.sin(A+counter)
	# /debug
	
	B = []
	
	try:
		B = oszi.read(cSIZE_DATA)
	except:
		ps.close()
		quit()
	
	if(len(B) == 0):
		print("Communication break down, baby I'm gonna leave you...")
		quit()
	else:
		#B_split = re.findall('..', B)
		
		B_split = [ B[i:i+2] for i in range(0, len(B), 2) ]
		B_int = [ int(B_split[i][1])*256+int(B_split[i][0]) for i in range(0, len(B_split)-1) ]
		
		x = np.linspace(0, len(B_int)-1, len(B_int))
		
		#int(B_split[0][1].encode('hex'),16)*256+int(B_split[0][0].encode('hex'),16)
		#print(B_split)
		#print(B_int)
		
		line.set_data(x,B_int)
		
		plt.draw()
		plt.show()
		
		plt.pause(.0001)
		
		#plt.clf()
		#plt.plot(B_int, '-*')
		#plt.gca().set_ylim([-5,5000])
		#plt.draw()
		
		