#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

WiFiUDP Udp;

#define cN_BYTES        10008
#define cTIMEOUT_BYTES  800

unsigned int port = 6666;
char in_packet[16];

uint16_t num_bytes_sent = 0;
uint8_t sync_pin_snd = 2;
uint8_t sync_pin_rcv = 0;

uint8_t do_send_wifi = 0;

uint32_t t = 0;
uint32_t k = 0;

void setup()
{
  ESP.wdtDisable();
  
  WiFi.softAP("aSillyScope", "12345678");
  Udp.begin(port);
  delay(2000);

  Serial.begin(1152000);
  Serial.setTimeout(7);
  
  pinMode(sync_pin_rcv, INPUT_PULLUP);
  //attachInterrupt(digitalPinToInterrupt(sync_pin_rcv), receiveStuff, FALLING);
  
  pinMode(sync_pin_snd, OUTPUT);
  digitalWrite(sync_pin_snd, HIGH);

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
}

void loop()
{ 
  while(!digitalRead(sync_pin_rcv)); //ESP.wdtFeed();

  digitalWrite(LED_BUILTIN, LOW);
  
  Udp.beginPacket("192.168.4.2", 6666);

  while(k<cN_BYTES)
  {  
    // fetch data and shove it up wifi good:
    if(Serial.available())
    {
      Udp.write(Serial.read());
      k++;
      t=0;
    }
    else
    {
      if(++t>cTIMEOUT_BYTES)
      {
        continue;
      }
    }
  }

  if(k >= cN_BYTES)
  {
    Udp.endPacket();
  }

  k = 0;
  
  while(digitalRead(sync_pin_rcv)); // ESP.wdtFeed();
  digitalWrite(LED_BUILTIN, HIGH);
}
