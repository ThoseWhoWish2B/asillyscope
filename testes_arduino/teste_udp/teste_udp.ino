#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

#define cN_BYTES        10008
#define cTIMEOUT_BYTES  800

WiFiUDP Udp;

unsigned int port = 6666;
char in_packet[16];
char out_packet[cN_BYTES];

uint16_t num_bytes_rcvd = 0;
uint8_t sync_pin_snd = 2;
uint8_t sync_pin_rcv = 0;

uint8_t do_send_wifi = 0;

void setup()
{
  ESP.wdtDisable();
  
  WiFi.softAP("aSillyScope", "12345678");
  Udp.begin(port);
  delay(2000);

  Serial.begin(2000000);
  //Serial.begin(2764800);
  //Serial.begin(3000000);
  Serial.setTimeout(5);
  
  pinMode(sync_pin_rcv, INPUT_PULLUP);
  //attachInterrupt(digitalPinToInterrupt(sync_pin_rcv), receiveStuff, FALLING);
  
  pinMode(sync_pin_snd, OUTPUT);
  digitalWrite(sync_pin_snd, HIGH);

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  fillInOutPacket();
}

void loop()
{ 
  while(!digitalRead(sync_pin_rcv))
  {
    yield();
  }
  
  digitalWrite(LED_BUILTIN, LOW);
  //try and fetch some data:
  num_bytes_rcvd = Serial.readBytes(out_packet, sizeof(out_packet));
  digitalWrite(LED_BUILTIN, HIGH);
  
  if(num_bytes_rcvd >= cN_BYTES)
  {
    Udp.beginPacket("192.168.4.2", 6666);
    Udp.write(out_packet, cN_BYTES);
    //Udp.write(out_packet, num_bytes_rcvd);
    Udp.endPacket();
  }

  num_bytes_rcvd = 0;

  while(digitalRead(sync_pin_rcv))
  {
    yield();
  }
}

void fillInOutPacket()
{
	for(int i=0; i<sizeof(out_packet); i++)
	{
		out_packet[i] = i;
	}
}
