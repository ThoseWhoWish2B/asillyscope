#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

#define cSIZE_GENERAL_HEADER    16
#define cPTS_PER_SCRN   2500   // number of received bytes (2x points per screen)
const uint16_t cSIZE_CH_DATA = 2*cPTS_PER_SCRN;    // number of received bytes (2x points per screen)
const uint16_t cSIZE_UDP_PACKET = 2*cSIZE_CH_DATA + cSIZE_GENERAL_HEADER;    // number of received bytes (2x points per screen)

#define cTIMEOUT_BYTES  800

WiFiUDP Udp;

unsigned int port = 6666;
char out_packet[cSIZE_UDP_PACKET];

uint8_t sync_pin_snd = 2;
uint8_t sync_pin_rcv = 0; // Pin D3 in nodemcu

uint8_t can_bridge_up = 0;

void setup()
{
  //ESP.wdtDisable();
  
  WiFi.softAP("aSillyScope", "12345678");
  Udp.begin(port);
  delay(2000);

  Serial.begin(2000000);
  //Serial.begin(2764800);
  //Serial.begin(3000000);
  Serial.setTimeout(5);
  
  pinMode(sync_pin_rcv, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(sync_pin_rcv), syncUpISR, RISING);
  
  pinMode(sync_pin_snd, OUTPUT);
  digitalWrite(sync_pin_snd, HIGH);

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  fillInOutPacket();
}

void loop()
{ 
  if(can_bridge_up)
  {
    bridgeUp();
    can_bridge_up = 0;
  }
  if(Udp.parsePacket())
  {
    bridgeDown();  
  }
  yield();
}

void fillInOutPacket()
{
	for(int i=0; i<sizeof(out_packet); i++)
	{
		out_packet[i] = i;
	}
}

void bridgeUp()
{
  static uint16_t num_bytes_rcvd = 0;
  
  digitalWrite(LED_BUILTIN, LOW);
  //try and fetch some data:
  num_bytes_rcvd = Serial.readBytes(out_packet, sizeof(out_packet));
  digitalWrite(LED_BUILTIN, HIGH);
  
  if(num_bytes_rcvd >= cSIZE_UDP_PACKET)
  {
    Udp.beginPacket("192.168.4.2", 6666);
    //Udp.beginPacket("192.168.4.255", 6666);
    Udp.write(out_packet, cSIZE_UDP_PACKET);
    //Udp.write(out_packet, num_bytes_rcvd);
    Udp.endPacket();
  }

  num_bytes_rcvd = 0;
}

void bridgeDown()
{
  static uint16_t num_bytes_rcvd = 0;
  static uint8_t in_packet[2*cSIZE_GENERAL_HEADER];

  Udp.read(in_packet, cSIZE_GENERAL_HEADER);
  Serial.write(in_packet, cSIZE_GENERAL_HEADER);

  num_bytes_rcvd = 0;
}

void syncUpISR()
{
  can_bridge_up = 1;
}


