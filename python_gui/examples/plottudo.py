import numpy as np
import scipy
import matplotlib.pyplot as plt
import pandas as pd
import csv
import pyperclip

def plotaAGORAVAI():
    np.set_printoptions(threshold=np.nan)

    nomeArquivo = pyperclip.paste()
    #nomeArquivo = 'TesteVANT-18-05-2017-20.38'
    arquivoLogCorrigido = ('logs\\' + nomeArquivo + '_corrigido.csv')

    matrix = np.genfromtxt(arquivoLogCorrigido, delimiter='\t', skip_header=0, usecols=(0, 1))
    matrix = np.delete(matrix, (0), axis=0)
    pitch = np.array(matrix[:, 0])
    roll = np.array(matrix[:, 1])

    roll = pd.Series(roll).interpolate().values
    pitch = pd.Series(pitch).interpolate().values

    plt.subplot(212)
    plt.plot(roll)
    plt.title('Roll')
    plt.subplot(211)
    plt.plot(pitch)
    plt.title('Pitch')
    plt.show()

#plotaAGORAVAI()