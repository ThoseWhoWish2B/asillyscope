from socket import socket, AF_INET, SOCK_STREAM
from tkinter import *
import plottudo
import corrigearquivo
import pyperclip
import sys
from tkinter import messagebox
from threading import Thread, Timer
import time


flag_plot = True

print(time.strftime("Teste VANT %d/%m/%Y %H:%M", time.localtime()))
log_name = time.strftime("TesteVANT-%d-%m-%Y-%H.%M", time.localtime())
pyperclip.copy(log_name)
flag_break=0;
#janela
class Application:
    def __init__(self, master=None):

        self.fontePadrao = ("Arial", "10")

        photo = PhotoImage(file="lammi.png")
        label = Label(image=photo)
        label.image = photo  # keep a reference!
        label.pack(side=TOP)

        self.widget1 = Frame(master)
        self.widget1.pack()
		
        self.msg_box_title = Label(self.widget1, text="Veículo Aéreo Não Tripulado")
        self.msg_box_title["font"] = ("Arial", "20")
        self.msg_box_title.pack()

        self.msg_box_subtitle = Label(self.widget1, text="Para Monitoramento de Áreas Agricolas")
        self.msg_box_subtitle["font"] = ("Arial", "15")
        self.msg_box_subtitle.pack()

        self.pitch = Button(self.widget1)
        self.pitch["text"] = "Pitch"
        self.pitch["font"] = ("Calibri", "10")
        self.pitch["width"] = 20
        self.pitch["command"] = self.comando_pitch
        self.pitch.pack()


        self.roll = Button(self.widget1)
        self.roll["text"] = "Roll"
        self.roll["font"] = ("Calibri", "10")
        self.roll["width"] = 20
        self.roll["command"] = self.comando_roll
        self.roll.pack()

        self.widget2 = Frame(master)
        self.widget2.pack()

        self.yaw = Button(self.widget2)
        self.yaw["text"] = "Yaw"
        #self.yaw["text"]= "Roll +"
        self.yaw["font"] = ("Calibri", "10")
        self.yaw["width"] = 20
        self.yaw["command"] = self.comando_yaw
        self.yaw.pack()

        self.altura = Button(self.widget2)
        self.altura["text"] = "Realimentação de Altura"
        self.altura["font"] = ("Calibri", "10")
        self.altura["width"] = 20
        self.altura["command"] = self.comando_altura
        self.altura.pack()

        self.estabiliza = Button(self.widget2)
        self.estabiliza["text"] = "Estabiliza"
        self.estabiliza["font"] = ("Calibri", "10")
        self.estabiliza["width"] = 20
        self.estabiliza["command"] = self.comando_estabiliza
        self.estabiliza.pack()

        self.widgetBox = Frame(master)
        self.widgetBox.pack()

        self.quantizaLabel = Label(self.widgetBox, text="Quantizar Degrau:", font=self.fontePadrao)
        self.quantizaLabel.pack(side=LEFT)

        self.dim = Entry(self.widgetBox)
        self.dim["width"] = 3
        self.dim["font"] = self.fontePadrao
        self.dim.pack(side=LEFT)

        self.widget3 = Frame(master)
        self.widget3.pack()

        self.descida = Button(self.widget3)
        self.descida["text"] = "Degrau-Descida"
        self.descida["font"] = ("Calibri", "10")
        self.descida["width"] = 25
        self.descida["command"] = self.comando_descida
        self.descida.pack(side=RIGHT)

        self.subida = Button(self.widget3)
        self.subida["text"] = "Degrau-Subida"
        self.subida["font"] = ("Calibri", "10")
        self.subida["width"] = 25
        self.subida["command"] = self.comando_subida
        self.subida.pack(side=LEFT)

        self.widget4 = Frame(master)
        self.widget4.pack(side=TOP)

        self.espaço = Label(self.widget4,text="\n\n")
        self.espaço["font"] = ("Arial", "10")
        self.espaço.pack()

        self.pitchbox = Frame(master)
        self.pitchbox.pack(side=TOP)

        self.pup = Button(self.pitchbox)
        self.pup["text"] = "Pitch +"
        self.pup["font"] = ("Calibri", "10")
        self.pup["width"] = 25
        self.pup["command"] = self.comando_pitchup
        self.pup.pack(side=LEFT)

        self.pdown = Button(self.pitchbox)
        self.pdown["text"] = "Pitch -"
        self.pdown["font"] = ("Calibri", "10")
        self.pdown["width"] = 25
        self.pdown["command"] = self.comando_pitchdn
        self.pdown.pack(side=LEFT)

        self.rollbox = Frame(master)
        self.rollbox.pack(side=TOP)

        self.rup = Button(self.rollbox)
        self.rup["text"] = "Roll +"
        self.rup["font"] = ("Calibri", "10")
        self.rup["width"] = 25
        self.rup["command"] = self.comando_rollup
        self.rup.pack(side=LEFT)

        self.rdown = Button(self.rollbox)
        self.rdown["text"] = "Roll -"
        self.rdown["font"] = ("Calibri", "10")
        self.rdown["width"] = 25
        self.rdown["command"] = self.comando_rolldn
        self.rdown.pack(side=LEFT)

        self.yawbox = Frame(master)
        self.yawbox.pack(side=TOP)

        self.yup = Button(self.yawbox)
        self.yup["text"] = "Yaw +"
        self.yup["font"] = ("Calibri", "10")
        self.yup["width"] = 25
        self.yup["command"] = self.comando_yawup
        self.yup.pack(side=LEFT)

        self.ydown = Button(self.yawbox)
        self.ydown["text"] = "Yaw -"
        self.ydown["font"] = ("Calibri", "10")
        self.ydown["width"] = 25
        self.ydown["command"] = self.comando_yawdn
        self.ydown.pack(side=LEFT)

        self.boxbox = Frame(master)
        self.boxbox.pack()

        self.espaço2 = Label(self.boxbox, text="\n")
        self.espaço2["font"] = ("Arial", "7")
        self.espaço2.pack()

        self.log = Button(self.boxbox)
        self.log["text"] = "Gravar Arquivo"
        self.log["font"] = ("Calibri", "10")
        self.log["width"] = 35
        self.log["command"] = self.comando_log
        self.log.pack()

        self.espaço2 = Label(self.boxbox, text="\n")
        self.espaço2["font"] = ("Arial", "7")
        self.espaço2.pack()

        self.deuRuim = Button(self.boxbox)
        self.deuRuim["text"] = "Desligar Motores"
        self.deuRuim["font"] = ("Calibri", "12")
        self.deuRuim["width"] = 45
        self.deuRuim["command"] = self.comando_deuRuim
        self.deuRuim.pack()

        self.religar = Button(self.boxbox)
        self.religar["text"] = "Ligar Motores"
        self.religar["font"] = ("Calibri", "12")
        self.religar["width"] = 45
        self.religar["command"] = self.comando_religar
        self.religar.pack()

        self.widgetbottom = Frame(master)
        self.widgetbottom.pack(side=BOTTOM)

        self.rodape = Label(self.widgetbottom, text="\n\n\n\n\n\nLaboratório Avançado de Magnetismo, Medidas e Instrumentação\nPIBITI 2016/2017:\nProfessor: Marlio J. do Couto Bonfim\nAluno: João Victor Predebon\nLAMMI é melhor que GICS\nhttp://eletrica.ufpr.br/lammi")
        self.rodape["font"] = ("Arial", "5")
        self.rodape.pack(side=BOTTOM)


    def quantizaDegrau(self):
        quantidade = self.dim.get()
        if quantidade=='':
            quantidade='0'
        quantidade=int(quantidade)
        global quantiza_subida
        global quantiza_descida
        if quantidade<=0:
            quantiza_subida=''
            quantiza_descida=''
        if quantidade==1:
            quantiza_subida='m'
            quantiza_descida='n'
        if quantidade==2:
            quantiza_subida='mm'
            quantiza_descida='nn'
        if quantidade==3:
            quantiza_subida='mmm'
            quantiza_descida='nnn'
        if quantidade==4:
            quantiza_subida='mmmm'
            quantiza_descida='nnnn'
        if quantidade==5:
            quantiza_subida='mmmmm'
            quantiza_descida='nnnnn'
        if quantidade==6:
            quantiza_subida='mmmmmm'
            quantiza_descida='nnnnnn'
        if quantidade==7:
            quantiza_subida='mmmmmmm'
            quantiza_descida='nnnnnnn'
        if quantidade==8:
            quantiza_subida='mmmmmmmm'
            quantiza_descida='nnnnnnnn'
        if quantidade==9:
            quantiza_subida='mmmmmmmmm'
            quantiza_descida='nnnnnnnnn'
        if quantidade==10:
            quantiza_subida='mmmmmmmmmm'
            quantiza_descida='nnnnnnnnnn'
        if quantidade>=11:
            quantiza_subida='mmmmmmmmmmm'
            quantiza_descida='nnnnnnnnnnn'

    def comando_pitch(self):
        global msg
        msg= 'p'

    def comando_altura(self):
        global msg
        msg = 'q'

    def comando_roll(self):
        global msg
        msg = 'r' #r

    def comando_yaw(self):
        global msg
        msg = 'y'

    def comando_rollup(self):
        global msg
        msg = 'a'

    def comando_rolldn(self):
        global msg
        msg = 'b'
    def comando_pitchup(self):
        global msg
        msg = 'w'

    def comando_pitchdn(self):
        global msg
        msg = 's'

    def comando_yawup(self):
        global msg
        msg = 'd'

    def comando_yawdn(self):
        global msg
        msg = 'R'

    def comando_deuRuim(self):
        global msg
        msg = 'K'
        #mantemDesligado = Timer(5.0, self.comando_deuRuim)
        mantemDesligado.start()
        
    def comando_religar(self):
        global msg
        msg = 'G'
        #mantemDesligado = Timer(5.0, self.comando_religar)
        mantemDesligado.start()

    def comando_subida(self):
        global msg
        global quantiza_subida
        self.quantizaDegrau()
        msg = quantiza_subida

    def comando_descida(self):
        global msg
        global quantiza_descida
        self.quantizaDegrau()
        msg = quantiza_descida

    def comando_estabiliza(self):
        global msg
        msg = 'z'

    def comando_log(self):
        global log_file
        log_file.close()
        messagebox.showinfo("sucesso!","arquivo salvo com nome "+log_name+" na pasta de logs")
        corrigearquivo.corrige()
        if (flag_plot):
            plottudo.plotaAGORAVAI()

def loopdamortehotwheels():
    root = Tk()
    root.wm_title("VANT UFPR - PROGRAMA DE CALIBRAÇÃO")
    Application(root)
    root.mainloop()

def resetWatchDog():
    global msg
    msg = 'J'
    resetWDT = Timer(15.0, resetWatchDog)
    resetWDT.start()



PORT = 34543
# classe para manipular o socket
class Send:
    def __init__(self):
        self.__msg = ''
        self.new = True
        self.con = None

    def put(self, msg):
        self.__msg = msg
        if self.con != None:
            # envia um mensagem atravez de uma conexão socket
            self.con.send(str.encode(self.__msg))

    def get(self):
        return self.__msg

    def loop(self):
        return self.new

# função esperar - Thread
def esperar(tcp, send, host='', port=PORT):
    origem = (host, port)
    # cria um vinculo
    tcp.bind(origem)
    # deixa em espera
    tcp.listen(1)

    while True:
        # aceita um conexão
        global cliente
        con, cliente = tcp.accept()
        print('IP', cliente, 'conectado à porta',PORT, '\n\n\n\n\n')
        # atribui a conexão ao manipulador
        send.con = con
        global log_file
        log_file = open('logs\\'+log_name+'.txt', 'w')
        #messagebox.showinfo("veículo conectado", "conexão estabelecida em "+str(cliente))
        while True:

            # aceita uma mensagem
            global flag_cliente
            flag_cliente = 1
            msg = con.recv(1024)
            if not msg: break
            rx=str(msg, 'utf-8')
            print(rx)
            log_file.write(rx+'\n')


if __name__ == '__main__':
# cria um socket
    global msg
    tcp = socket(AF_INET, SOCK_STREAM)
    send = Send()
    # cria um Thread e usa a função esperar com dois argumentos
    processo = Thread(target=esperar, args=(tcp, send))
    #resetWDT = Thread (target= resetWatchDog, args())
    #resetWDT.start()
    resetWDT = Timer(15.0, resetWatchDog)
    resetWDT.start()
    processo.start()
    interface = Thread(target=loopdamortehotwheels, args=())
    print('\nPIBITI 2016/2017\nLaboratório Avançado de Magnetismo, Medidas e Instrumentação\nProfessor: Marlio J. do Couto Bonfim\nAluno: João Victor Predebon\n\nServidor pronto para iniciar a conexão com o veículo\n\n')
    msg = ''#input()
    interface.start()
    while True:

            if msg == 'break':
                break
            elif msg:
                send.put(msg)
                msg = ''


    sys.close(0)
    processo.join()
    interface.join()
    tcp.close()
    exit()