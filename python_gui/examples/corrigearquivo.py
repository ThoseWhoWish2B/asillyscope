import csv
import pyperclip

NUM_CHAR =14

def corrige():
    #print ("digite o nome do log que deseja corrigir:")
    #nomeArquivo=input()
    nomeArquivo = pyperclip.paste();
    arquivoLog = open('logs\\'+nomeArquivo+".txt",'r')
    arquivoLogCorrigido = open('logs\\'+nomeArquivo+'-corrigido.csv','w')

    #arquivoLog = open ('C:\\Users\\jao\\Desktop\\'+'Novo Documento de Texto.txt', 'r')
    linha = arquivoLog.readline() #le a primeira linha

    while linha!='':
       # arquivoLog.readline()
        n = sum(c != '' for c in linha)# conta o numero de caracteres na linha
        print ('n:' + str(n))
        if n < NUM_CHAR:# se for menor que num_char:
            linhaAnterior = linha.rstrip('\n')
            linhaProxima = arquivoLog.readline()
            linhaSoma= linhaAnterior+linhaProxima
            m = sum(c != '' for c in linhaSoma)# forma uma nova linha que pode ou nao estar certa
            print ('m:'+str(m))
            if m==NUM_CHAR: #verifica se é certa
                linha=linhaSoma
            else:
                linha=linhaAnterior+'\n'+linhaProxima

                       #linha_display=linha.rstrip()# tira mais algum \n que possa ter
        if (n!=1):
            arquivoLogCorrigido.write(linha)# e adiciona apenas no final
        linha=arquivoLog.readline()#proxima linha

    arquivoLog.close()
    arquivoLogCorrigido.close()

#corrige()