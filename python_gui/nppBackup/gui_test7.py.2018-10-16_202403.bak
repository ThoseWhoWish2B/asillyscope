import tkinter as tk
from tkinter import ttk
import matplotlib
import threading as td
import numpy as np
import serial as ps
matplotlib.use("TkAgg")

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.figure import Figure
from matplotlib import style

style.use("dark_background")

def CallbackRunStop():

	if b_single.config('relief')[-1] == "sunken":
		b_single.config(relief="raised")
	else:
		if b_run_stop.config('relief')[-1] == "raised":
			b_run_stop.config(relief="sunken")
			print("run!")
		else:
			b_run_stop.config(relief="raised")
			print("stop!")

def CallbackSingle():	
	if b_run_stop.config('relief')[-1] == "sunken":
		b_run_stop.config(relief="raised")

	if b_single.config('relief')[-1] == "raised":
		b_single.config(relief="sunken")
		print("single!")
	else:
		b_single.config(relief="raised")

def CallbackBlackBG():
	if b_black_bg.config('relief')[-1] == "raised":
		b_black_bg.config(relief="sunken")
	else:
		b_black_bg.config(relief="raised")

def CallbackKeyPressed(event)
	print("key!")
	print(repr(event.char))
		
row_count = 0
		
win = tk.Tk()
win.title("A Silly Scope")
tk.Grid.rowconfigure(win, 0, weight=1)
tk.Grid.columnconfigure(win, 0, weight=1)

#win.bind_all("<1>", lambda event: event.widget.focus_set())
win.bind_all("<1>", lambda event: event.widget.focus_set())
win.bind_all("<Key>", CallbackKeyPressed())

trigger_source = tk.IntVar()
ch1_coupling = tk.IntVar()
ch2_coupling = tk.IntVar()
ch1_active = tk.IntVar()
ch2_active = tk.IntVar()
ch1_dc_coupled = tk.IntVar()
ch2_dc_coupled = tk.IntVar()

# graph stuff
fig = Figure(figsize=(5,5), dpi=100)
sub_plt = fig.add_subplot(111)
x = [1,2,3,4,5,6,7,8,9,10]
line_ch1, = sub_plt.plot(x,[1,2,3,4,5,6,7,8,9,10])

# Bunch of frames:

# frame for graph
f_graph = tk.Frame(win) 
f_graph.grid(column = 0, row = 0, sticky = tk.N + tk.S + tk.E + tk.W)
tk.Grid.rowconfigure(f_graph, 0, weight=1)
tk.Grid.columnconfigure(f_graph, 0, weight=1)

# frame for dashboard
f_dashboard = tk.Frame(win) 
f_dashboard.grid(column = 1, row = 0, sticky = tk.N)
tk.Grid.rowconfigure(f_dashboard, 0, weight=1)
tk.Grid.columnconfigure(f_dashboard, 0, weight=1)

# subframe for matplotlib toolbar (to separate pack from grid...) 
sf_toolbar = tk.Frame(f_graph)
sf_toolbar.grid(column = 0, row = 1, sticky = tk.W)

# subframe for general controls
sf_gen_ctrls = tk.Frame(f_dashboard)
sf_gen_ctrls.grid(column = 0, row = 0, columnspan = 2, sticky = tk.N, pady=(20,10))
tk.Grid.rowconfigure(sf_gen_ctrls, 0, weight=1)
tk.Grid.columnconfigure(sf_gen_ctrls, 0, weight=1)

# Populate frames:

# Plot
row_count=0
canvas = FigureCanvasTkAgg(fig, f_graph)
canvas.draw()
canvas.get_tk_widget().grid(row=row_count, column=0, sticky = tk.N + tk.S + tk.E + tk.W)

toolbar=NavigationToolbar2Tk(canvas, sf_toolbar).pack()

# General Controls:
row_count += 1
b_run_stop = tk.Button(sf_gen_ctrls, text="Run/Stop", command=CallbackRunStop)
b_run_stop.grid(row=row_count, column=0, columnspan = 2, sticky = tk.N + tk.S + tk.E + tk.W)

row_count += 1
b_single = tk.Button(sf_gen_ctrls, text="Single", command=CallbackSingle)
b_single.grid(row=row_count, column=0, columnspan = 2, sticky = tk.N + tk.S + tk.E + tk.W)

# Time scale
row_count += 1
l_timescale = tk.Label(sf_gen_ctrls,  text="Time scale:")
l_timescale.grid(row=row_count, column=0, sticky = tk.W, pady=(3,0))
row_count += 1
ssf_timescale = tk.Frame(sf_gen_ctrls)
ssf_timescale.grid(row = row_count, column = 0, columnspan = 2, sticky = tk.N)
sb_timescale = tk.Spinbox(ssf_timescale, from_=0, to=10, increment=.01, repeatinterval=2, repeatdelay=200, width=4)
sb_timescale.bind("<Return>", lambda event: win.focus())
sb_timescale.grid(row=0, column=0, sticky = tk.N)
sb_timescale = ttk.Combobox(ssf_timescale, values=["ns", "µs", "ms", "s"] , width=3)
sb_timescale.bind("<Return>", lambda event: win.focus())
sb_timescale.grid(row=0, column=1, sticky = tk.N)
sb_timescale.current(2)
l_timescale = tk.Label(ssf_timescale,  text="/div")
l_timescale.grid(row=0, column=2, sticky = tk.E)

# Trigger
row_count += 1
l_trigger = tk.Label(sf_gen_ctrls,  text="Trigger:")
l_trigger.grid(row=row_count, column=0, sticky = tk.W, pady=(3,0))
tk.Grid.columnconfigure(b_single, 0, weight=1)

# trigger source
row_count += 1
ssf_trg_source = tk.Frame(sf_gen_ctrls)
ssf_trg_source.grid(row = row_count, column = 0, columnspan = 2, sticky = tk.E + tk.W)
tk.Grid.columnconfigure(ssf_trg_source, 0, weight=1)
tk.Grid.rowconfigure(ssf_trg_source, 0, weight=1)
l_source = tk.Label(ssf_trg_source,  text="source")
l_source.grid(row=0, column=0, sticky = tk.W)
rb_ch1 = tk.Radiobutton(ssf_trg_source, text="ch1", variable=trigger_source, value=0, indicatoron=0)
rb_ch1.select()
rb_ch1.grid(row=0, column=1, sticky = tk.E)
rb_ch2 = tk.Radiobutton(ssf_trg_source, text="ch2", variable=trigger_source, value=1, indicatoron=0)
rb_ch2.grid(row=0, column=2, sticky = tk.E)

row_count += 1
l_trigger_level = tk.Label(sf_gen_ctrls,  text="level")
l_trigger_level.grid(row=row_count, column=0, sticky = tk.W)
sb_trigger_level = tk.Spinbox(sf_gen_ctrls, from_=0, to=10, increment=.01, repeatinterval=2, repeatdelay=200, width=4)
sb_trigger_level.bind("<Return>", lambda event: win.focus())
sb_trigger_level.grid(row=row_count, column=1, sticky = tk.N)

row_count += 1
l_trigger_pos = tk.Label(sf_gen_ctrls,  text="position")
l_trigger_pos.grid(row=row_count, column=0, sticky = tk.W)
sb_trigger_pos = tk.Spinbox(sf_gen_ctrls, from_=0, to=10, increment=.01, repeatinterval=2, repeatdelay=200, width=4)
sb_trigger_pos.bind("<Return>", lambda event: win.focus())
sb_trigger_pos.grid(row=row_count, column=1, sticky = tk.N)


# CH1 Specific Controls:

# subframe for CH1 controls
sf_ch_ctrls_ch1 = tk.Frame(f_dashboard)
sf_ch_ctrls_ch1.grid(column = 0, row = 1, sticky = tk.N + tk.E + tk.W, padx=(5,5), pady=(10,10))
tk.Grid.rowconfigure(sf_ch_ctrls_ch1, 0, weight=1)
tk.Grid.columnconfigure(sf_ch_ctrls_ch1, 0, weight=1)

# CH1 Label
row_count = 0
l_ch1 = tk.Label(sf_ch_ctrls_ch1, text="CH1")
l_ch1.grid(row=row_count, column=1, sticky = tk.W)

# active
row_count += 1
ssf_ch1_active = tk.Frame(sf_ch_ctrls_ch1)
ssf_ch1_active.grid(row=row_count, column=1, sticky = tk.W)

tk.Grid.columnconfigure(ssf_ch1_active, 0, weight=1)
rb_ch1_on = tk.Radiobutton(ssf_ch1_active, text="ON", variable=ch1_active, value=1, width=3, indicatoron=0)
rb_ch1_on.select()
rb_ch1_on.grid(row=row_count, column=0, sticky = tk.W)
rb_ch1_off = tk.Radiobutton(ssf_ch1_active, text="OFF", variable=ch1_active, value=0, width=3, indicatoron=0)
rb_ch1_off.grid(row=row_count, column=1, sticky = tk.W)

# coupling
row_count += 1
ssf_ch1_ac_dc = tk.Frame(sf_ch_ctrls_ch1)
ssf_ch1_ac_dc.grid(row=row_count, column=1, sticky = tk.W)

tk.Grid.columnconfigure(ssf_ch1_ac_dc, 0, weight=1)
rb_ch1_dc = tk.Radiobutton(ssf_ch1_ac_dc, text="DC", variable=ch1_dc_coupled, value=1, state="disabled", width=3, indicatoron=0)
rb_ch1_dc.select()
rb_ch1_dc.grid(row=row_count, column=0, sticky = tk.W)
rb_ch1_ac = tk.Radiobutton(ssf_ch1_ac_dc, text="AC", variable=ch1_dc_coupled, value=0, state="disabled", width=3, indicatoron=0)
rb_ch1_ac.grid(row=row_count, column=1, sticky = tk.W)

# vertical scale
row_count += 1
l_ch1_scale = tk.Label(sf_ch_ctrls_ch1, text="scale")
l_ch1_scale.grid(row=row_count, column=0, sticky = tk.W)

ssf_ch1_scale = tk.Frame(sf_ch_ctrls_ch1)
ssf_ch1_scale.grid(row=row_count, column=1)

sb_scale_ch1 = tk.Spinbox(ssf_ch1_scale, from_=0, to=1000, increment=.01, repeatinterval=2, repeatdelay=200, width=4)
sb_scale_ch1.bind("<Return>", lambda event: win.focus())
sb_scale_ch1.grid(row=0, column=0, sticky = tk.N)
sb_scale_ch1 = ttk.Combobox(ssf_ch1_scale, values=["nV", "µV", "mV", "V", "kV"] , width=3)
sb_scale_ch1.bind("<Return>", lambda event: win.focus())
sb_scale_ch1.grid(row=0, column=1, sticky = tk.N)
sb_scale_ch1.current(2)
l_scale_ch1 = tk.Label(ssf_ch1_scale,  text="/div")
l_scale_ch1.grid(row=0, column=2, sticky = tk.E)

# offset
row_count += 1
l_ch1_offset = tk.Label(sf_ch_ctrls_ch1, text="offset")
l_ch1_offset.grid(row=row_count, column=0, sticky = tk.W)
ssf_ch1_offset = tk.Frame(sf_ch_ctrls_ch1)
ssf_ch1_offset.grid(row=row_count, column=1)
sb_offset_ch1 = tk.Spinbox(ssf_ch1_offset, from_=0, to=1000, increment=.01, repeatinterval=2, repeatdelay=200, width=4)
sb_offset_ch1.bind("<Return>", lambda event: win.focus())
sb_offset_ch1.grid(row=0, column=0, sticky = tk.N)
sb_offset_ch1 = ttk.Combobox(ssf_ch1_offset, values=["nV", "µV", "mV", "V", "kV"] , width=3)
sb_offset_ch1.bind("<Return>", lambda event: win.focus())
sb_offset_ch1.grid(row=0, column=1, sticky = tk.N)
sb_offset_ch1.current(2)
l_ch1_offset_ch1 = tk.Label(ssf_ch1_offset,  text="/div")
l_ch1_offset_ch1.grid(row=0, column=2, sticky = tk.E)

# CH2 Specific Controls:

# subframe for CH2 controls
sf_ch_ctrls_ch2 = tk.Frame(f_dashboard)
sf_ch_ctrls_ch2.grid(column = 0, row = 12, sticky = tk.N + tk.E + tk.W, padx=(5,5), pady=(10, 10))
tk.Grid.rowconfigure(sf_ch_ctrls_ch2, 0, weight=1)
tk.Grid.columnconfigure(sf_ch_ctrls_ch2, 0, weight=1)

# CH2 Label
row_count = 0
l_ch2 = tk.Label(sf_ch_ctrls_ch2, text="CH2")
l_ch2.grid(row=row_count, column=1, sticky = tk.W)

# active
row_count += 1
ssf_ch2_active = tk.Frame(sf_ch_ctrls_ch2)
ssf_ch2_active.grid(row=row_count, column=1, sticky = tk.W)
tk.Grid.columnconfigure(ssf_ch2_active, 0, weight=1)

rb_ch2_on = tk.Radiobutton(ssf_ch2_active, text="ON", variable=ch2_active, value=1, width=3, indicatoron=0)
rb_ch2_on.select()
rb_ch2_on.grid(row=row_count, column=0, sticky = tk.W)
rb_ch2_off = tk.Radiobutton(ssf_ch2_active, text="OFF", variable=ch2_active, value=0, width=3, indicatoron=0)
rb_ch2_off.grid(row=row_count, column=1, sticky = tk.W)

# coupling
row_count += 1
ssf_ch2_ac_dc = tk.Frame(sf_ch_ctrls_ch2)
ssf_ch2_ac_dc.grid(row=row_count, column=1, sticky = tk.W)

tk.Grid.columnconfigure(ssf_ch2_ac_dc, 0, weight=1)
rb_ch2_dc = tk.Radiobutton(ssf_ch2_ac_dc, text="DC", variable=ch2_dc_coupled, value=1, state="disabled", width=3, indicatoron=0)
rb_ch2_dc.select()
rb_ch2_dc.grid(row=row_count, column=0, sticky = tk.W)
rb_ch2_ac = tk.Radiobutton(ssf_ch2_ac_dc, text="AC", variable=ch2_dc_coupled, value=0, state="disabled", width=3, indicatoron=0)
rb_ch2_ac.grid(row=row_count, column=1, sticky = tk.W)

# vertical scale
row_count += 1
l_ch2_scale = tk.Label(sf_ch_ctrls_ch2, text="scale")
l_ch2_scale.grid(row=row_count, column=0, sticky = tk.W)
ssf_ch2_scale = tk.Frame(sf_ch_ctrls_ch2)
ssf_ch2_scale.grid(row=row_count, column=1)
sb_scale_ch2 = tk.Spinbox(ssf_ch2_scale, from_=0, to=1000, increment=.01, repeatinterval=2, repeatdelay=200, width=4)
sb_scale_ch2.bind("<Return>", lambda event: win.focus())
sb_scale_ch2.grid(row=0, column=0, sticky = tk.N)
sb_scale_ch2 = ttk.Combobox(ssf_ch2_scale, values=["nV", "µV", "mV", "V", "kV"] , width=3)
sb_scale_ch2.bind("<Return>", lambda event: win.focus())
sb_scale_ch2.grid(row=0, column=1, sticky = tk.N)
sb_scale_ch2.current(2)
l_scale_ch2 = tk.Label(ssf_ch2_scale,  text="/div")
l_scale_ch2.grid(row=0, column=2, sticky = tk.E)

# offset
row_count += 1
l_ch2_offset = tk.Label(sf_ch_ctrls_ch2, text="offset")
l_ch2_offset.grid(row=row_count, column=0, sticky = tk.W)
ssf_ch2_offset = tk.Frame(sf_ch_ctrls_ch2)
ssf_ch2_offset.grid(row=row_count, column=1)
sb_offset_ch2 = tk.Spinbox(ssf_ch2_offset, from_=0, to=1000, increment=.01, repeatinterval=2, repeatdelay=200, width=4)
sb_offset_ch2.bind("<Return>", lambda event: win.focus())
sb_offset_ch2.grid(row=0, column=0, sticky = tk.N)
sb_offset_ch2 = ttk.Combobox(ssf_ch2_offset, values=["nV", "µV", "mV", "V", "kV"] , width=3)
sb_offset_ch2.bind("<Return>", lambda event: win.focus())
sb_offset_ch2.grid(row=0, column=1, sticky = tk.N)
sb_offset_ch2.current(2)
l_offset_ch2 = tk.Label(ssf_ch2_offset,  text="/div")
l_offset_ch2.grid(row=0, column=2, sticky = tk.E)


# Thread test:
my_value = 42
def test_thread():
	print("it works!")
	print(my_value)
	td.Timer(1, test_thread).start()

do_it = True

# Thread test2:
def test_thread2():
	global do_it
	do_it = not do_it
	if(do_it):
		line_ch1.set_data(x,[1,2,3,4,5,6,7,8,9,10])
	else:
		line_ch1.set_data(x,[10,9,8,7,6,5,4,3,2,1])
	canvas.draw()
	td.Timer(.01, test_thread2).start()

# Thread test:
td_test = td.Thread(target=test_thread2, args=())
td_test.daemon = True
td_test.start()


win.mainloop()
