import tkinter as tk
import matplotlib
matplotlib.use("TkAgg")

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
from matplotlib import style

style.use("dark_background")

def CallbackRunStop():

	if b_single.config('relief')[-1] == "sunken":
		b_single.config(relief="raised")
	else:
		if b_run_stop.config('relief')[-1] == "raised":
			b_run_stop.config(relief="sunken")
			print("run!")
		else:
			b_run_stop.config(relief="raised")
			print("stop!")

def CallbackSingle():	
	if b_run_stop.config('relief')[-1] == "sunken":
		b_run_stop.config(relief="raised")

	if b_single.config('relief')[-1] == "raised":
		b_single.config(relief="sunken")
		print("single!")
	else:
		b_single.config(relief="raised")

def CallbackBlackBG():
	if b_black_bg.config('relief')[-1] == "raised":
		b_black_bg.config(relief="sunken")
	else:
		b_black_bg.config(relief="raised")
		
win = tk.Tk()

# graph stuff
fig = Figure(figsize=(5,5), dpi=100)
sub_plt = fig.add_subplot(111)
sub_plt.plot([1,2,3,4,5,6,7,8,9,10],[1,2,3,4,5,6,7,8,9,10])

canvas = FigureCanvasTkAgg(fig, win)
canvas.show()
canvas.get_tk_widget().pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

toolbar = NavigationToolbar2TkAgg(canvas, win)
toolbar.update()
canvas._tkcanvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

# button stuff
b_run_stop = tk.Button(win, text="Run/Stop", command=CallbackRunStop)
b_run_stop.pack(side=tk.TOP)

b_single = tk.Button(win, text="Single", command=CallbackSingle)
b_single.pack(side=tk.TOP)

sb_trigger_level = tk.Spinbox(win, text="Trigger Level", from_=0, to=10, increment=.01, repeatinterval=10, repeatdelay=200)
sb_trigger_level.pack(side=tk.TOP)

b_black_bg = tk.Button(win, text="Dark", command=CallbackBlackBG)
b_black_bg.pack(side=tk.BOTTOM)


win.mainloop()
