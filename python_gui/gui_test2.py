import tkinter as tk

def CallbackRunStop():

	if b_single.config('relief')[-1] == "sunken":
		b_single.config(relief="raised")
	else:
		if b_run_stop.config('relief')[-1] == "raised":
			b_run_stop.config(relief="sunken")
			print("run!")
		else:
			b_run_stop.config(relief="raised")
			print("stop!")

def CallbackSingle():	
	if b_single.config('relief')[-1] == "raised":
		b_single.config(relief="sunken")
		print("single!")
	else:
		b_single.config(relief="raised")

win = tk.Tk()

b_run_stop = tk.Button(win, text="Run/Stop", command=CallbackRunStop)
b_run_stop.pack()

b_single = tk.Button(win, text="Single", command=CallbackSingle)
b_single.pack()

sb_trigger_level = tk.Spinbox(win, text="Trigger Level", from_=0, to=10, increment=.01, repeatinterval=10, repeatdelay=200)
sb_trigger_level.pack()

win.mainloop()
